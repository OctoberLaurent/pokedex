<?php

namespace App\Pokedex;

/*
 *  Class Pokemon
 */
class Pokemon
{
    private int $id;
    private string $name;
    private string $description;
    private float $weight;
    private float $height;
    private string $category;
    private array $ability;
    private array $types;
    private string $pictureUrl;
    private string $generation;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    /**
     * @param float $height
     */
    public function setHeight(float $height): void
    {
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return array
     */
    public function getAbility(): array
    {
        return $this->ability;
    }

    /**
     * @param array $ability
     */
    public function setAbility(array $ability): void
    {
        $this->ability = $ability;
    }

    /**
     * @return string
     */
    public function getGeneration(): string
    {
        return $this->generation;
    }

    /**
     * @param string $generation
     */
    public function setGeneration(string $generation): void
    {
        $this->generation = $generation;
    }

    /**
     * @return string
     */
    public function getPictureUrl(): string
    {
        return $this->pictureUrl;
    }

    /**
     * @param string $pictureUrl
     */
    public function setPictureUrl(string $pictureUrl): void
    {
        $this->pictureUrl = $pictureUrl;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array $types
     */
    public function setTypes(array $types): void
    {
        $this->types = $types;
    }

    public function serialize() : array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'pictureUrl' => $this->pictureUrl
        ];
    }

}