<?php

namespace App\Controller;

use App\Service\PokemonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PokemonController extends AbstractController
{

    /**
     * Show list of 12 pokemons
     *
     * @Route("/", name="home")
     * @Route("/{page}", name="home")
     */
    public function index(PokemonService $pokemonService, string $page = "1"): Response
    {
        $gen = $pokemonService->getAllIdOfPokemonInOneGeneration();

        $generationName = $gen["name"];
        $ids = $gen["ids"];

        $nbPokemonInGeneration = (count($ids)) + 1;
        $first = ($page * 12) - 12;
        $last = ($page * 12 > $nbPokemonInGeneration) ? $nbPokemonInGeneration - 1 : $page * 12;
        $pages = ($nbPokemonInGeneration) / 12;

        $pokemons = [];
        for ($i = $first; $i < $last; $i++) {
            $pokemons[] = $pokemonService->createPokemon($ids[$i]);
        }

        return $this->render('home/index.html.twig', [
            'pokemons' => $pokemons,
            'pages' => $pages,
            'curent' => $page,
            'generationName' => $generationName
        ]);
    }

    /**
     * Show one Pokemon
     *
     * @Route("/show/{id}", name="show")
     */
    public function show(int $id, PokemonService $pokemonService): Response
    {
        $pokemon = $pokemonService->createPokemon($id);

        return $this->render('home/show.html.twig', [
            'pokemon' => $pokemon
        ]);
    }
}
