<?php

namespace App\Service;

use App\Pokedex\Pokemon;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PokemonService
{
    private HttpClientInterface $client;


    public function __construct(
        HttpClientInterface $client,
        ParameterBagInterface $params
    )
    {
        $this->client = $client;
        $this->params = $params;
    }

    public function createPokemon(int $id): Pokemon
    {
        $informations = $this->getSinglePokemonInformation($id);

        $species = $this->getInfo($informations["species"]["url"]);

        $types = [];
        foreach ($informations["types"] as $type ){
            $types[] = ($this->getInfo($type["type"]["url"]))["names"][3]["name"];
        }

        $abilities = [];
        foreach ($informations["abilities"] as $ability ){
            $abilities[] = ($this->getInfo($ability["ability"]["url"]))["names"][3]["name"];
        }

        $pokemon = new Pokemon();
        $pokemon->setId($id);
        $pokemon->setName($species["names"][4]["name"]?? "");
        $pokemon->setDescription($species["flavor_text_entries"][77]["flavor_text"] ?? "");
        $pokemon->setWeight($informations["weight"] / 10);
        $pokemon->setHeight($informations["height"] / 10);
        $pokemon->setTypes($types);
        $pokemon->setCategory($species["genera"][3]["genus"] ?? "");
        $pokemon->setAbility($abilities);
        $pokemon->setGeneration($species["genera"][3]["genus"] ?? "");
        $pokemon->setPictureUrl($informations["sprites"]["front_shiny"] ?? "");

        return $pokemon;
    }

    private function getSinglePokemonInformation(int $item): array
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://pokeapi.co/api/v2/pokemon/' . $item
            );

            $pokemon = $response->toArray();

            return $pokemon;

        } catch (HttpException $e) {
            return ['error http' => $e];
        }
    }

    public function getAllIdOfPokemonInOneGeneration(): array
    {
        $generation = $this->params->get('generation');

        try {

            $response = $this->client->request(
                'GET',
                'https://pokeapi.co/api/v2/generation/' . $generation
            );

            $content = $response->toArray();

            $pokemons = $content["pokemon_species"];
            $frenchGenerationName = ($content["names"][2]["name"]);

            $gen = ["name" => $frenchGenerationName];
            foreach ($pokemons as $pokemon) {
                $gen["ids"][] = explode("/", $pokemon["url"])[6];
            }

            return $gen;

        } catch (HttpException $e) {
            return ['error' => $e];
        }
    }

    /*
     * retrieve informations of a request
     */
    private function getInfo($url): array
    {
        try {
            $response = $this->client->request(
                'GET',
                $url
            );
        } catch (HttpException $e) {
            return ['error' => $e];
        }

        return $response->toArray();
    }

}