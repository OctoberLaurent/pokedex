<?php

namespace App\Command;

use App\Service\PokemonService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/*
 * generate a csv file of a pokemeon generation with the symfony console PokemonCsv command
 */
class PokemonCsvCommand extends Command
{
    protected static $defaultName = 'PokemonCsv';
    protected static $defaultDescription = 'Make csv with list of pokemons';

    public function __construct(
        PokemonService $pokemonService,
        Filesystem $filesystem,
        ParameterBagInterface $params,
        string         $name = null
    )
    {
        parent::__construct($name);
        $this->pokemonService = $pokemonService;
        $this->filesystem = $filesystem;
        $this->projectRoot = $params->get('projectRoot');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $allIdsInGeneration = $this->pokemonService->getAllIdOfPokemonInOneGeneration();

        foreach ($allIdsInGeneration["ids"] as $id) {
            $pokemons[] = $this->pokemonService->createPokemon($id)->serialize();
        }

        $csv = $serializer->encode($pokemons, 'csv');

        $directory = $this->projectRoot.'/pokemonsCSV';

        if (!$this->filesystem->exists($directory)){
            try {
                $this->filesystem->mkdir($directory, 0775);
                $this->filesystem->chown($directory, "www-data");
                $this->filesystem->chgrp($directory, "www-data");
            } catch (IOExceptionInterface $exception) {
                $io->error("An error occurred while creating your directory at " . $exception->getPath());
                return Command::FAILURE;
            }
        }

        try {

            $name = 'pokemon-'. $allIdsInGeneration["name"] .'.csv';
            $fileName = $directory.'/'.$name;
            $this->filesystem->touch($fileName);
            $this->filesystem->chmod($fileName, 0777);
            $this->filesystem->dumpFile($fileName, $csv);

        } catch (IOExceptionInterface $exception) {
            $io->error("An error occurred while creating your directory at " . $exception->getPath());
            return Command::FAILURE;
        }

        $io->success('Your csv file has been generated !');

        return Command::SUCCESS;
    }
}
